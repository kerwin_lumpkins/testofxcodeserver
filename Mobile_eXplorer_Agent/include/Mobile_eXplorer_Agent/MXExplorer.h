//
//  MXHotSpot.h
//  MeXplorer
//
//  Created by Nghia Dang on 2/20/14.
//  Copyright (c) 2014 KMS Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@class MXSession;

typedef enum
{
    MXThemeDark,
    MXThemeLight
} MXTheme;

typedef enum
{
    MXExplorerStateNew = 0,
    MXExplorerStateLoggedIn,
    MXExplorerStateRecording,
    MXExplorerStatePausing,
    MXExplorerStateStopping,
    MXExplorerStateRecordingAudio
} MXExplorerState;

@interface MXExplorer : NSObject

@property (nonatomic, assign) MXTheme theme;
@property (nonatomic, strong) NSString *projectId;
@property (nonatomic, strong) NSString *projectName;
@property (nonatomic, strong) NSString *eXplorerVersion;

- (void)start;
- (BOOL)webView:(UIWebView *)webView handleJsRequest:(NSString *)requestString;

+ (MXExplorer *)sharedInstance;

@end

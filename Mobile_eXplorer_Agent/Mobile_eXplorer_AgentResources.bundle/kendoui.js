var ctrlMaps_KENDOUI = ctrlMaps_KENDOUI ||
	[
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'SELECT',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'km-label',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-text',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-group-title',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'km-text',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-group-title',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'km-group-title',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-pager-nav',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-current-page',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-pager-nav',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-icon',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-pager-nav',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-input',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-dropdown',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 1, //textbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-input',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-numeric',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 11, //tab
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-button',
							operator: 3 //string contain
						},
						{
							name: 'data-role',
							value: 'tab',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
						{
							navigate: 3,//first
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-text',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
					{
						navigate: 3,//first
						loop: 1
					},
				],
				properties:
				[
					{
						name: 'class',
						value: 'km-text',
						operator: 3 //string contain
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 11, //tab
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-button',
							operator: 3 //string contain
						},
						{
							name: 'data-role',
							value: 'tab',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 11, //tab
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'km-button',
							operator: 3 //string contain
						},
						{
							name: 'data-role',
							value: 'tab',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'km-listview-link',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-listview-link',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'km-button',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-button',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-button',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'km-switch',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-collapsible-header',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 3
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-collapsible-header',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-collapsible-header',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'km-collapsible-header',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-draghandle',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-slider',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'km-legend-button',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'checkbox',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 3,//first
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'checkbox',
							operator: 1 //string equal
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'km-checkbox-label',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 2, //radio
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 3,//first
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'radio',
							operator: 1 //string equal
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 2, //radio
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'radio',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 2, //radio
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'km-radio-label',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 9, //menuitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-link',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-item',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-menu',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 9, //menuitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-link',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-item',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-menu',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-header',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-item',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-panelbar',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-panelbar-collapse',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-panelbar-expand',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-button',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-icon',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-button',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-image',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-button',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 11, //tab
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'aria-controls',
							value: 'tabstrip',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-tabstrip',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 11, //tab
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-image',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'aria-controls',
							value: 'tabstrip',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-tabstrip',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 11, //tab
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-sprite',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'aria-controls',
							value: 'tabstrip',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-tabstrip',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 1, //textbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-input',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-checkbox-label',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 2, //radio
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-radio-label',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 1, //textbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-textbox',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 1, //textbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'TEXTAREA',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-textbox',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 1, //textbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-textbox',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-icon',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-textbox',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-icon',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-grid-filter',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-grid-filter',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 3,//first
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-icon',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'checkbox',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 3,//first
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'checkbox',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-i-arrow-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-link',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-link',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'TD',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'TR',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'TBODY',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 4
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'TABLE',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-content',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 5
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-calendar',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-nav-today',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-footer',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-calendar',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-nav',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-header',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-calendar',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-link',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 3,//first
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-i-arrow-',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 3,//first
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'UL',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-pager-numbers',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-icon',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'role',
							value: 'treeitem',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-item',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 8, //treeitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-in',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'role',
							value: 'treeitem',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-item',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 8, //treeitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-sprite',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-in',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'role',
							value: 'treeitem',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-item',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-i-arrow-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-select',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-dropdown',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'SPAN',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-dropdown',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-item',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-list',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-list-container',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-item',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-list',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-list-container',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'A',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'launchButton',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-i-calendar',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-select',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-i-clock',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-select',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-item',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-list',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-select',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-picker',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-i-arrow-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-select',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-picker',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-tool-icon',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-tool',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-tool',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-tool-group',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-i-close',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-select',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-button',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'id',
							value: 'files',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-button',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-link',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-view-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-gantt-views',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-link',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-nav',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-scheduler-navigation',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-nav-',
							operator: 3 //string contain
						},
					],
				},
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-link',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-link',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-view-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-header',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'SPAN',
							operator: 3 //string contain
						},
						{
							name: 'class',
							value: 'k-i-arrow-',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-si-close',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-collapse',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-splitbar',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-expand',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-splitbar',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-icon',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-window-action',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 13, //link
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'A',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'P',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'SPAN',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-item',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'UL',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-list',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'SPAN',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-item',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'UL',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-list',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'SPAN',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'sales-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'A',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-header-column-menu',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'A',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'k-header-column-menu',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'k-check',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'k-check',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'DIV',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'km-detail',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 14, //image
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'photo',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		
		{
			ctrlType: 7, //list item
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'P',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 7,//previous sibling
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'IMG',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'km-thumbnail',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		
		{
			ctrlType: 7, //list item
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'h4',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 7,//previous sibling
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'IMG',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'km-thumbnail',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
	];

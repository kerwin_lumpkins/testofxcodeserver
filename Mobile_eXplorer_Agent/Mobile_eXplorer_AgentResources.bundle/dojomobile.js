var ctrlMaps_DOJOMOBILE = ctrlMaps_DOJOMOBILE ||
	[
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'DIV',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'mblValuePickerSlotButton',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 3
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'DIV',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'mblValuePickerSlotButton',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'DIV',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'mblValuePickerSlotButton',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblValuePickerSlotButton',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'mblListItem',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'mblListItem',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'mblListItem',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
					{
						navigate: 3,//first
						loop: 1
					},
				],
				properties:
				[
					{
						name: 'class',
						value: 'mblListItemLabel',
						operator: 3 //string contain
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'lockButton',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblSliderHandle',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblSpriteIcon',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'mblToolBarButton',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblImageIcon',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'mblToolBarButton',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblToolBarButton',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'mblDomButton',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblDomButton',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblToggleButton',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblButton',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblSpriteIcon',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'mblAccordion',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 3
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblAccordion',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'button',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblSwitchText',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'mblSwitch',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblSwitchKnob',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblCheckBox',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
					{
						navigate: 3,//first
						loop: 1
					},
				],
				properties:
				[
					{
						name: 'tag',
						value: 'label',
						operator: 1 //string equal
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 1, //textbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblTextBox',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
					{
						navigate: 3,//first
						loop: 1
					},
				],
				properties:
				[
					{
						name: 'tag',
						value: 'label',
						operator: 1 //string equal
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 1, //textbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblTextArea',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
					{
						navigate: 3,//first
						loop: 1
					},
				],
				properties:
				[
					{
						name: 'tag',
						value: 'label',
						operator: 1 //string equal
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 2, //radio
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblRadioButton',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 2
					},
					{
						navigate: 3,//first
						loop: 1
					},
				],
				properties:
				[
					{
						name: 'tag',
						value: 'label',
						operator: 1 //string equal
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 11, //tab
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblTabBarButton',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 11, //tab
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'mblSpriteIcon',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'mblTabBarButton',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 1, //textbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 3 //string contain
						},
						{
							name: 'type',
							value: 'text',
							operator: 3 //string contain
						},
						{
							name: 'id',
							value: 'val',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'DIV',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'widgetid',
							value: 'badges',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
	];

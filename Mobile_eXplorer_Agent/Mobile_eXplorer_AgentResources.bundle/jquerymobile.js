var ctrlMaps_JQUERYMOBILE = ctrlMaps_JQUERYMOBILE ||
	[
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'date',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'month',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'week',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'time',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'datetime',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 4, //combobox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'datetime-local',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'checkbox',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'ui-checkbox',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'ui-checkbox',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 3, //checkbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'DIV',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'ui-checkbox',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 2, //radio
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LABEL',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'DIV',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'ui-radio',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 11, //tab
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'ui-tab',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'A',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'ui-icon-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'ui-li-has-alt',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'ui-btn',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 3
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'ui-navbar',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'ui-li',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'ui-btn',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'ui-li-',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'ui-li-has-',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 1,//parent
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 7, //listitem
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'ui-collapsible-heading',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 2
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'ui-li',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'DIV',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'ui-flipswitch',
							operator: 3 //string contain
						},
					],
				},
			],
			label:
			{
				navigates:
				[
					{
						navigate: 3,//first
						loop: 1
					},
				],
				name: 'innerText',
			},
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'DIV',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'ui-flipswitch',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'ui-btn-icon-notext',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'ui-btn',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'BUTTON',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'button',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'submit',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
						{
							name: 'type',
							value: 'reset',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'SPAN',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'tag',
							value: 'LI',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'ui-btn',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 0, //button
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'SPAN',
							operator: 1 //string equal
						},
						{
							name: 'class',
							value: 'filterable-select',
							operator: 3 //string contain
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'ui-btn',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 13, //link
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'jqm-deeplink',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 13, //link
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'ui-link',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 1, //textbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'class',
							value: 'ui-slider-input',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 1, //textbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'INPUT',
							operator: 1 //string equal
						},
					],
				},
				{
					navigates:
					[
						{
							navigate: 1,//parent
							loop: 1
						},
					],
					properties:
					[
						{
							name: 'class',
							value: 'ui-input',
							operator: 3 //string contain
						},
					],
				},
			],
		},
		{
			ctrlType: 1, //textbox
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'TEXTAREA',
							operator: 1 //string equal
						},
					],
				},
			],
		},
		{
			ctrlType: 14, //image
			elements:
			[
				{
					properties:
					[
						{
							name: 'tag',
							value: 'IMG',
							operator: 1 //string equal
						},
					],
				},
			],
		},
	];

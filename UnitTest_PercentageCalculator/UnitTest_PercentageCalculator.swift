//
//  UnitTest_PercentageCalculatorTests.swift
//  UnitTest_PercentageCalculatorTests
//
//  Created by Kerwin Lumpkins on 2/21/17.
//  Copyright © 2017 App Coda. All rights reserved.
//

import XCTest

@testable import PercentageCalculator

class UnitTest_PercentageCalculatorBasicFunction: XCTestCase {
    
    var vc:ViewController!
    
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name:"Main",bundle:Bundle.main )
        vc = storyboard.instantiateInitialViewController() as! ViewController
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPercentCalculation() {
        let p = vc.percentage(50,50)
        // added this comment
        XCTAssertEqual (p,25)

    }
    
    func testLabelValuesShowedProperly() {
        // test to update labels
        let _ = vc.view
        vc.updateLabels(Float(80.0), Float(50.0), Float(40.0))
        
        XCTAssert (vc.numberLabel.text == "80.0", "numberLabel does not show correct value")
        XCTAssert (vc.percentageLabel.text == "50.0%", "percentageLabel does not show correct value")
        XCTAssert (vc.resultLabel.text == "40.0", "resultLabel does not show correct value")
    }
    
    
}

//
//  UI_PercentageCalculatorTests.swift
//  UI_PercentageCalculatorTests
//
//  Created by Kerwin Lumpkins on 2/21/17.
//  Copyright © 2017 App Coda. All rights reserved.
//

import XCTest

class UI_PercentageCalculatorBasicFunction: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testChangeNumberSlider() {
        
        let app = XCUIApplication()
        app.sliders["acid.slider.number"].adjust(toNormalizedSliderPosition: 0.40)
        app.sliders["acid.slider.percent"].adjust(toNormalizedSliderPosition: 0.11)
        
        sleep (6)
        
        
        print (app.debugDescription)
        
        let result = app.staticTexts["acid.label.resultNumber"].label
        
        XCTAssertEqual(result, "4.0", "result value was not as expected")
        
    }
    
    func testker () {
        
        let app = XCUIApplication()
        app.sliders["acid.slider.number"].swipeLeft()
        app.staticTexts["acid.label.resultNumber"].tap()
        app.sliders["acid.slider.percent"].tap()
        
    }
    
}
